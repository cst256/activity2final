<?php

namespace App\Http\Controllers;
use App\Models\UserModel;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use App\Services\Business\SecurityService;

class LoginController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    public function index(Request $request){
        $username = $request->input('username');
        $password = $request->input('password');
        
        $usrModel = new UserModel();
        $usrModel->setUsername($username);
        $usrModel->setPassword($password);
        $ss = new SecurityService();
        $isLoggedIn = $ss->login($usrModel);
        
        if ($isLoggedIn){
            return view('loginPassed2')->with('model', $usrModel);
        } else {
            return view('loginFailed');
        }
        
        echo "The username passed was ".$username." and the password passed was ".$password;
        echo "<br>";
    }
}
